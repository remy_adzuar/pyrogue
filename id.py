import pygame

class Id:
    """
    Regroupe les ID du jeu
    """

    def __init__(self) -> None:
        self.id_list = {
            1:{"img":pygame.image.load("./imgs/grass.png"),
            "name":"grass"},
            2:{"img": pygame.image.load("./imgs/water.png"),
            "name": "water"},
            3:{"img":pygame.image.load("./imgs/char1.png"),
            "name": "player"}
        }
    
    def get_id(self,id):
        if id in self.id_list.keys():
            return self.id_list[id]
    
    def get_img(self, id):
        if id in self.id_list.keys():
            return self.id_list[id]["img"]
    

if __name__ == "__main__":

    i = Id()
    i.get_id(2)
    i.get_img(2)