import pygame

from config import Config
from entity_manager import Entity_Manager

class Main:

    def __init__(self) -> None:
        
        self.version = "0.00"
        self.print_version()

        self.config = Config()
        self.EM = Entity_Manager(self.config)
        self.EM.map.load_from_file("./maps/map1.txt")

        self.load()

        loop = True
        while loop:
            self.update()
            self.draw()
            self.process_key()

            pygame.display.update()

    def load(self):
        pass

    def update(self):
        self.EM.update()

    def draw(self):
        self.EM.draw()

    def process_key(self):
        self.EM.process_key()

    def print_version(self):
        print("PyRogue")
        print("Version "+self.version)

if __name__ == "__main__":

    Main()