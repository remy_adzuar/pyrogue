import pygame

from entity import *
from map import Map
from id import Id
from config import Config

class Entity_Manager:

    def __init__(self, config:Config) -> None:
        self.config = config
        self.id = Id()
        self.map = Map(self.config)
        self.entities = [
        ]
        self.player = Player()
        self.player.set_pos(1,1)
        self.items = [

        ]
        self.enemies = [

        ]

    def add_entity(self, entity):
        if isinstance(entity, Player):
            # self.entities.append(entity)
            self.player = entity
        if isinstance(entity, Item):
            # self.entities.append(entity)
            self.items.append(entity)
        if isinstance(entity, Slime):
            # self.entities.append(entity)
            self.enemies.append(entity)
    
    def update(self):

        for enemy in self.enemies:
            pass
        for item in self.items:
            pass
        self.map.update(self.player)
        self.player.update()

    def draw(self):

        self.map.draw()
        self.config.get_screen().blit(self.id.get_img(3), (self.player.x*16,self.player.y*16))

        pass

    def process_key(self):
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    self.player.move(-1,0)
                if event.key == pygame.K_RIGHT:
                    self.player.move(1,0)
                if event.key == pygame.K_UP:
                    self.player.move(0,-1)
                if event.key == pygame.K_DOWN:
                    self.player.move(0,1)