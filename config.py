import pygame

class Config:

    def __init__(self) -> None:
        self.screen = pygame.display.set_mode((1280,720))
        self.clock = pygame.time.Clock()
        self.tile_size = 16

    def get_screen(self) -> pygame.Surface:
        return self.screen

    def get_clock(self) -> pygame.time.Clock:
        return self.clock