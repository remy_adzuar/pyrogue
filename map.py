import pygame

from id import Id
from config import Config
from entity import Player

class Map:
    """
    Classe qui definit une map...
    """

    def __init__(self, config:Config) -> None:
        self.id = Id()
        self.config = config
        self.width = 50
        self.height = 25
        self.matrix = [[None for i in range(self.width)] for i in range(self.height)]
    
    def get_line(self, pos) -> list():
        if pos >= 0 and pos < self.height:
            return self.matrix[pos]
        return []
    
    def get_case(self, x:int, y:int):
        if x >= 0 and x < self.width:
            if y>= 0 and y < self.height:
                return self.matrix[y][x]
        return None
    
    def set_case(self, x:int, y:int, value):
        if x >= 0 and x < self.width:
            if y >= 0 and y < self.height:
                self.matrix[y][x] = value
    
    def filling(self, value):

        y = 0
        while y < self.height:
            x = 0
            while x < self.width:

                self.set_case(x,y, value)
                x += 1
            y += 1
    
    def load_from_file(self, URL):
        file = open(URL, "r")
        self.matrix = []
        for line in file:
            line_strip = line.strip()
            line_split = line_strip.split(",")
            line_int = [int(x) for x in line_split]
            self.matrix.append(line_int)
        self.height = len(self.matrix)
        if self.height > 0:
            self.width = len(self.matrix[0])
    
    def check_collide(self, pos, id):
        if self.get_case(pos[0],pos[1]) == id:
            return True
        return False
    
    def check_collide_void(self, pos):
        if self.get_case(pos[0],pos[1]) == None:
            return True
        return False

    def print_console(self):
        for line in self.matrix:
            print(line)
    
    def update(self, player:Player):
        p_pos = player.get_pos()
        collide_water = self.check_collide(p_pos, 2)
        collide_void = self.check_collide_void(p_pos)
        if collide_water or collide_void:
            l_pos = player.last_pos
            player.set_pos(l_pos[0], l_pos[1])


    def draw(self):
        ts = self.config.tile_size
        y = 0
        while y < self.height:
            x = 0
            while x < self.width:
                surf = self.get_case(x,y)
                if surf != None:
                    self.config.get_screen().blit(self.id.get_img(surf), (ts*x, ts*y))
                x += 1
            y += 1

if __name__ == "__main__":

    map1 = Map(Config())
    map1.filling(1)
    map1.print_console()
    map1.load_from_file("./maps/map1.txt")
    map1.print_console()