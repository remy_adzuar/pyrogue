import pygame

from id import Id
from config import Config

class Entity:
    
    def __init__(self) -> None:
        self.x = 0
        self.y = 0
        self.last_pos = (self.x,self.y)
    
    def get_pos(self):
        return [self.x, self.y]
    
    def set_pos(self, x:int, y:int):
        self.last_pos = self.get_pos()
        self.x = x
        self.y = y
    
    def move(self, x:int, y:int):
        self.last_pos = self.get_pos()
        self.x += x
        self.y += y
    
class Player(Entity):

    def __init__(self,) -> None:
        super().__init__()

    def update(self):
        pass

    def draw(self, config:Config):
        return 3

class Item(Entity):

    def __init__(self) -> None:
        super().__init__()

class Slime(Entity):

    def __init__(self) -> None:
        super().__init__()